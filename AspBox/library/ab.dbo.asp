<%
'######################################################################
'## ab.dbo.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Database Operation
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/08/17 12:05
'## Description :   AspBox Database Operation Block
'######################################################################

Class Cls_AB_Dbo

	Private errid,errdes,errsou
	Private strTable

	Private Sub Class_Initialize
		AB.Use "db"
	End Sub
	Private Sub Class_Terminate:End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.Table 属性 (可读/可写)
	'# @return:  --
	'# @dowhat:  设置当前数据表名，此属性可读可写
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "User" : AB.C.Print AB.Dbo.Table
	'------------------------------------------------------------------------------------------

	Public Property Let Table(byval s)
		strTable = AB.db.FixSQL("{prefix}"&AB.db.DelFix(s))
	End Property

	Public Property Get Table()
		Table = strTable
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.Exist(Field, Value)
	'# @return: Boolean (布尔值)
	'# @dowhat: 判断数据是否存在
	'--DESC------------------------------------------------------------------------------------
	'# @param Field: [array] 字段名
	'# @param Value: [array] 字段值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "User" : AB.C.Print AB.Dbo.Exist("id", 99)
	'# AB.Dbo.Table = "User" : AB.C.Print AB.Dbo.Exist("[name]", "'Lajox'")
	'# AB.Dbo.Table = "User" : AB.C.Print AB.Dbo.Exist("artist", "Blue")
	'------------------------------------------------------------------------------------------

	Public Function Exist(byval Field ,byval Value)
		Dim Sql,Rs,i
		If VarType(Value) = 8 Then '字符串
			Value = Trim(Value)
			Value = Trim(AB.C.RegReplace(Value, "^'([^']*)'$", "$1"))
			Value = "'" & Value & "'"
		End If
		Sql="select * from "&strTable&" where "&Field&"="&Value
		On Error Resume Next
		Set Rs = AB.db.GRS(Sql)
		IF Rs.Eof Then:Exist = False:Else:Exist = True:End IF
		Rs.close:Set Rs = nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.Add(Field, Value)
	'# @return: Boolean (布尔值) 添加成功返回 True, 添加失败返回 False
	'# @dowhat: 添加一条数据
	'--DESC------------------------------------------------------------------------------------
	'# @param Field: [array / string] 字段名
	'# @param Value: [array / string] 字段值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : AB.Dbo.Add Array("field1","field2"), Array("value1","value2")
	'# AB.Dbo.Table = "test01" : AB.Dbo.Add "field1", "value1"
	'------------------------------------------------------------------------------------------

	Public Function Add(byval Field , byval Value)
		Dim Sql,Rs,i
		On Error Resume Next
		Add = False
		IF IsArray(Field) And IsArray(Value) Then
			IF AB.A.CheckArray(Field,Value,0) = False Then AB.C.Put "Fun Add()数组参数错误":End IF
		End If
		Sql="select * from "&strTable
		Set Rs=AB.db.ExeC(Sql)
		Rs.addnew
		IF IsArray(Field) And IsArray(Value) Then '都是数组
			For i = 0 to ubound(Field)
				Rs(Field(i)&"") = Value(i)
			Next
		ElseIf Not IsArray(Field) And Not IsArray(Value) Then '都不是数组
			Rs(Field&"") = Value
		ElseIf IsArray(Field) And Not IsArray(Value) Then '前数组，后字符串
			For i = 0 to ubound(Field)
				Rs(Field(i)&"") = Value
			Next
		End If
		Rs.Update
		If Err.Number=0 Then Add = True:Rs.Close:Set Rs = nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------------------
	'# AB.Dbo.Update(Field, Value, KeyStr)
	'# @return: Boolean (布尔值) 更新成功返回 True, 更新失败返回 False
	'# @dowhat: 更新数据
	'--DESC------------------------------------------------------------------------------------------------
	'# @param Field: [array / string] 字段名
	'# @param Value: [array / string] 字段值
	'# @param KeyStr: [string] 查询条件
	'--DEMO------------------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : AB.Dbo.Update Array("field1","field2"), Array("value1","value2"), "id=99"
	'# AB.Dbo.Table = "test01" : AB.Dbo.Update "field1", "value1", "id=99"
	'------------------------------------------------------------------------------------------------------

	Public Function Update(byval Field , byval Value ,byval KeyStr)
		Dim Sql,Rs,i
		On Error Resume Next
		Update = False
		IF IsArray(Field) And IsArray(sValue) Then
			IF AB.A.CheckArray(Field,Value,0) = False Then AB.C.Put "Fun Update()数组参数错误":End IF
		End If
		Sql = "select * from "&strTable&" where "&KeyStr
		Set Rs = AB.db.ExeC(Sql)
		IF IsArray(Field) And IsArray(sValue) Then '都是数组
			For i = 0 to ubound(Field)
				Rs(Field(i))=sValue(i)
			Next
		ElseIf Not IsArray(Field) And Not IsArray(sValue) Then '都不是数组
			Rs(Field&"") = sValue
		ElseIf IsArray(Field) And Not IsArray(sValue) Then '前数组，后字符串
			For i = 0 to ubound(Field)
				Rs(Field(i)&"") = sValue
			Next
		End If
		Rs.Update
		If Err.Number=0 Then Update = True:Rs.close:Set Rs = nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------------------
	'# AB.Dbo.Del 方法
	'# @syntax: AB.Dbo.Del(KeyStr)
	'# @return: Boolean (布尔值) 删除成功返回 True, 删除失败返回 False
	'# @dowhat: 删除数据
	'--DESC------------------------------------------------------------------------------------------------
	'# @param KeyStr: [string] 查询条件
	'--DEMO------------------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : AB.Dbo.Del "id=99"
	'------------------------------------------------------------------------------------------------------

	Public Function Del(byval KeyStr)
		Dim Sql,temp
		On Error Resume Next
		Del = False
		If Not IsNull(KeyStr) and Trim(KeyStr)<>"" Then temp = " where "&KeyStr
		Sql = "delete from "&strTable&""&temp : AB.db.ExeC(Sql)
		If Err.Number=0 Then Del = True
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.Open 方法
	'# @syntax:  AB.Dbo.Open(sql)
	'# @return:  Object (ASP对象) 返回原始 RecordSet 记录集对象
	'# @dowhat:  以只读方式打开SQL记录集
	'--DESC------------------------------------------------------------------------------------
	'# @param sql: String (字符串) 要查询的SQL语句
	'--DEMO------------------------------------------------------------------------------------
	'# Dim Rs : Set Rs = AB.Dbo.Open("SELECT * FROM test01 WHERE id<10") : AB.C.Print Rs.RecordCount
	'------------------------------------------------------------------------------------------

	Public Function Open(byval sql)
		sql = Trim(sql)
		On Error Resume Next
		Dim i : i = AB.db.QueryType
		AB.db.QueryType = 0
		Set Open = AB.db.GRS(sql)
		AB.db.QueryType = i
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.Open3 方法
	'# @syntax:  AB.Dbo.Open3(sql)
	'# @return:  Object (ASP对象) 返回原始 RecordSet 记录集对象
	'# @dowhat:  以可更新方式打开SQL记录集
	'--DESC------------------------------------------------------------------------------------
	'# @param sql: String (字符串) 要查询的SQL语句
	'--DEMO------------------------------------------------------------------------------------
	'# Dim Rs : Set Rs = AB.Dbo.Open3("SELECT * FROM test01 WHERE id<10")
	'# Rs("field1") = "test111"
	'# Rs.Update '更新Rs记录集
	'------------------------------------------------------------------------------------------

	Public Function Open3(byval sql)
		sql = Trim(sql)
		On Error Resume Next
		Set Open3 = AB.db.Run(sql,"rst3")
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.ExeC 方法
	'# @syntax: [Set rs = ] AB.Dbo.ExeC(sql)
	'# @return: Object (ASP对象) 返回原始 RecordSet 记录集对象
	'# @dowhat: 执行SQL语句, 并返回RecordSet记录集对象
	'--DESC------------------------------------------------------------------------------------
	'# @param sql: String (字符串) 要查询的SQL语句
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.ExeC "DELETE * FROM test01 WHERE id=99"
	'# Dim Rs : Set Rs=AB.Dbo.ExeC("SELECT * FROM test01 WHERE id<10")
	'# Dim RsCount : RsCount = Rs.RecordCount
	'# Dim arrRs : If Not Rs.Eof Then arrRs = Rs.GetRows()
	'# 'If IsArray(arrRs) Then RsCount = Ubound(arrRs,2)+1
	'# AB.C.PrintCn "总记录数：" & RsCount
	'# If IsArray(arrRs) Then AB.Trace arrRs
	'------------------------------------------------------------------------------------------

	Public Function ExeC(byval sql)
		On Error Resume Next
		sql = Trim(sql)
		If Lcase(Left(sql,6)) = "select" Then
			Set ExeC = AB.db.ExeC(sql)
		Else
			AB.db.ExeC(sql)
		End If
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.GetRs 方法
	'# @syntax: [Set rs = ] AB.Dbo.GetRs(FieldName, TopNum, KeyStr)
	'# @return: Object (ASP对象) 返回原始 RecordSet 记录集对象
	'# @dowhat: (以只读方式)按条件获得Rs数据(RecordSet记录集)
	'--DESC------------------------------------------------------------------------------------
	'# @param FieldName: String (字符串) 设置要读取的字段，为空则读取全部字段
	'# @param TopNum: Integer (整数) 前几条数据，0 或 "" 则读取全部
	'# @param KeyStr: String (字符串) 查询条件(关键字)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : Set Rs = AB.Dbo.GetRs("", 0, "id=10") : AB.C.Print Rs("id")
	'# AB.Dbo.Table = "test01" : Set Rs = AB.Dbo.GetRs("id,name", 10, "") : AB.C.Print Rs.RecordCount
	'------------------------------------------------------------------------------------------

	Public Function GetRs(byval FieldName, byval TopNum, byval KeyStr)
		IF FieldName = "" Then FieldName = "*":End IF
		IF TopNum = 0 Then TopNum = "" Else TopNum = "top "&TopNum:End IF
		IF KeyStr <> "" Then
			KeyStr = Trim(KeyStr)
			IF Lcase(Left(KeyStr,5)) = "order" And Trim(mid(KeyStr,6,1)) = "" Then
				KeyStr = " "&KeyStr
			Else
				KeyStr = " where "&KeyStr
			End IF
		End IF
		On Error Resume Next
		Dim Sql : Sql = "select "&TopNum&" "&FieldName&" from "&strTable&KeyStr
		Set GetRs = AB.db.Run(sql,"rst1")
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.GetRs3 方法
	'# @syntax: [Set rs = ] AB.Dbo.GetRs3(FieldName, TopNum, KeyStr)
	'# @return: Object (ASP对象) 返回原始 RecordSet 记录集对象
	'# @dowhat: (按可更新方式)按条件获得Rs数据(RecordSet记录集)
	'--DESC------------------------------------------------------------------------------------
	'# @param FieldName: String (字符串) 设置要读取的字段，为空则读取全部字段
	'# @param TopNum: Integer (整数) 前几条数据，0 或 "" 则读取全部
	'# @param KeyStr: String (字符串) 查询条件(关键字)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : Set Rs = AB.Dbo.GetRs3("", 0, "id=10") : AB.C.Print Rs("id")
	'# AB.Dbo.Table = "test01" : Set Rs = AB.Dbo.GetRs3("id,name", 10, "") : AB.C.Print Rs.RecordCount
	'------------------------------------------------------------------------------------------

	Public Function GetRs3(byval FieldName , byval TopNum ,byval KeyStr)
		IF FieldName = "" Then FieldName = "*":End IF
		IF TopNum = 0 Then TopNum = "" Else TopNum = "top "&TopNum:End IF
		IF KeyStr <> "" Then
			KeyStr = Trim(KeyStr)
			IF Lcase(Left(KeyStr,5)) = "order" And Trim(mid(KeyStr,6,1)) = "" Then
				KeyStr = " "&KeyStr
			Else
				KeyStr = " where "&KeyStr
			End IF
		End IF
		On Error Resume Next
		Dim Sql : Sql = "select "&TopNum&" "&FieldName&" from "&strTable&KeyStr
		Set GetRs3 = AB.db.Run(Sql,"rst3")
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.RsArr 方法
	'# @syntax:  [Set rs = ] AB.Dbo.RsArr(FieldName, TopNum, KeyStr)
	'# @return:  Array (数组) 取得数组数据(二维)
	'# @dowhat:  按条件取得数据(数组形式)
	'--DESC------------------------------------------------------------------------------------
	'# @param FieldName: String (字符串) 设置要读取的字段，为空则读取全部字段
	'# @param TopNum: Integer (整数) 前几条数据，0 或 "" 则读取全部
	'# @param KeyStr: String (字符串) 查询条件(关键字)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : arrRs = AB.Dbo.RsArr("*", 10, "") : If IsArray(arrRs) Then AB.C.Print Ubound(arrRs,2)+1
	'# AB.Dbo.Table = "test01" : arrRs = AB.Dbo.RsArr("id,name", 0, "id<10") : If IsArray(arrRs) Then AB.C.Print arrRs(1,0)
	'------------------------------------------------------------------------------------------

	Public Function RsArr(byval FieldName , byval TopNum ,byval KeyStr)
		IF FieldName = "" Then FieldName = "*":End IF
		IF TopNum = 0 Then TopNum = "" Else TopNum = "top "&TopNum:End IF
		IF KeyStr <> "" Then
			KeyStr = Trim(KeyStr)
			IF Lcase(Left(KeyStr,5)) = "order" And Trim(mid(KeyStr,6,1)) = "" Then
				KeyStr = " "&KeyStr
			Else
				KeyStr = " where "&KeyStr
			End IF
		End IF
		On Error Resume Next
		Dim Sql : Sql = "select "&TopNum&" "&FieldName&" from "&strTable&KeyStr
		RsArr = AB.db.Run(Sql,"arr")
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.RsArr 方法
	'# @syntax: [Set rs = ] AB.Dbo.RsArr(FieldName, TopNum, KeyStr)
	'# @return: Object (对象) Discionary 对象
	'# @dowhat: 按条件取得数据(字典形式)，将记录的RS转换为Discionary，以实现断开连接的RecordSet
	'--DESC------------------------------------------------------------------------------------
	'# @param FieldName: String (字符串) 设置要读取的字段，为空则读取全部字段
	'# @param TopNum: Integer (整数) 前几条数据，0 或 "" 则读取全部
	'# @param KeyStr: String (字符串) 查询条件(关键字)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Dbo.Table = "test01" : Set dictRs = AB.Dbo.RsDict("*", 10, "") : AB.C.Print dictRs("id")
	'# AB.Dbo.Table = "test01" : Set dictRs = AB.Dbo.RsDict("id,name", 0, "id<10") : AB.C.Print dictRs("name")
	'------------------------------------------------------------------------------------------

	Public Function RsDict(byval FieldName , byval TopNum ,byval KeyStr)
		On Error Resume Next
		IF FieldName = "" Then FieldName = "*":End IF
		IF TopNum = 0 Then TopNum = "" Else TopNum = "top "&TopNum:End IF
		IF KeyStr <> "" Then
			KeyStr = Trim(KeyStr)
			IF Lcase(Left(KeyStr,5)) = "order" And Trim(mid(KeyStr,6,1)) = "" Then
				KeyStr = " "&KeyStr
			Else
				KeyStr = " where "&KeyStr
			End IF
		End IF
		Dim Sql : Sql = "select "&TopNum&" "&FieldName&" from "&strTable&KeyStr
		Dim oRs,oDict,i
	    Set oDict = Server.CreateObject(AB.DictName)
	    Set oRs = AB.db.Run(Sql,"rst1")
	    If Err.Number<>0 Then
			oDict.Add "errCode",Err.Number
	        oDict.Add "errDesc",Err.Description
	        oDict.Add "errSource",Err.Source
	        Set RsDict = oDict
			If Err Then Err.Clear
	        Exit Function
	    End If
	    If Not oRs.Eof Then
	        For i=0 to oRs.fields.count-1
	            oDict.Add oRs.fields(i).name, oRs.fields(i).value
	        Next
	    End If
		Set RsDict = oDict
	    Set oDict = Nothing
	    oRs.Close():Set oRs = Nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.RsUpdate 方法
	'# @syntax: AB.Dbo.RsUpdate(RsObj, sFields, sValues)
	'# @return: void
	'# @dowhat: 批量更新数据库记录Rs值
	'# 			分隔符(:)之后表示字段类型(int:整型，str:字符串，date:时间)
	'--DESC------------------------------------------------------------------------------------
	'# @param RsObj: Object (RecordSet对象) 可更新的 RecordSet 对象
	'# @param sFields: String | Array (字符串 或 数组) 更新字段名
	'# @param sValues: String | Array (字符串 或 数组) 更新字段值
	'--DEMO------------------------------------------------------------------------------------
	'# Dim Rs : Set Rs = AB.Dbo.Open3("SELECT * FROM test01 WHERE id=99")
	'# Call AB.Dbo.RsUpdate(Rs, "name,artist", "名称,艺术家")
	'# Call AB.Dbo.RsUpdate(Rs, "name,hits", Array("名称",Rs("hits")+1))
	'# Call AB.Dbo.RsUpdate(Rs, "title,hits", "aaaaa:str,5:int")
	'# Call AB.Dbo.RsUpdate(Rs, Array("title,hits"), Array("aaaaa",5))
	'------------------------------------------------------------------------------------------

	Public Function RsUpdate(Byval RsObj, Byval sFields, Byval sValues)
		On Error Resume Next
		IF IsNull(RsObj)=True Or IsObject(RsObj)=False Or Lcase(TypeName(RsObj))<>"recordset" Then Exit Function
		Call SetRs(RsObj,sFields,sValues)
		RsObj.Update
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Dbo.SetRs 方法
	'# @syntax: AB.Dbo.SetRs(RsObj, sFields, sValues)
	'# @return: void
	'# @dowhat: 批量设置数据库记录Rs值
	'# 			此方法和 AB.Dbo.RsUpdate 唯一区别在于：此方法仅设置Rs值，但不做更新
	'# 			分隔符(:)之后表示字段类型(int:整型，str:字符串，date:时间)
	'--DESC------------------------------------------------------------------------------------
	'# @param RsObj: Object (RecordSet对象) 可更新的 RecordSet 对象
	'# @param sFields: String | Array (字符串 或 数组) 更新字段名
	'# @param sValues: String | Array (字符串 或 数组) 更新字段值
	'--DEMO------------------------------------------------------------------------------------
	'# Dim Rs : Set Rs = AB.Dbo.Open3("SELECT * FROM test01 WHERE id=99")
	'# Call AB.Dbo.SetRs(Rs, "name,artist", "名称,艺术家")
	'# Call AB.Dbo.SetRs(Rs, "name,hits", Array("名称",Rs("hits")+1))
	'# Call AB.Dbo.SetRs(Rs, "title,hits", "aaaaa:str,5:int")
	'# Call AB.Dbo.SetRs(Rs, Array("title,hits"), Array("aaaaa",5))
	'------------------------------------------------------------------------------------------

	Public Function SetRs(Byval RsObj,Byval sFields,Byval sValues)
		On Error Resume Next
		IF IsNull(RsObj)=True Or IsObject(RsObj)=False Or Lcase(TypeName(RsObj))<>"recordset" Then Exit Function
		IF IsNull(sFields) Or IsEmpty(sFields) Then Exit Function
		If Lcase(TypeName(sFields))="string" Then If Trim(sFields)="" Then Exit Function
		Dim s1,s2:s1=sFields:s2=sValues
		Dim a1,a2,a3,i,j,k:i=0:j=0:k=0
		Dim pt:pt="str"
		Dim x,y,z
		IF IsArray(s1) And IsArray(s2) Then 'arr1和arr2都是数组
			If Ubound(s1)<0 Or Ubound(s2)<0 Or Err Then:Err.Clear:Exit Function:End If
			a1 = s1 : a2 = s2
			IF UBound(a1)<=UBound(a2) Then
				For i=0 To UBound(a1)
					RsObj(a1(i)&"") = a2(i)
				Next
			Else
				For i=0 To UBound(a2)
					RsObj(a1(i)&"") = a2(i)
				Next
				For i=UBound(a2)+1 To UBound(a1)
					RsObj(a1(i)&"") = ""
				Next
			End IF
		Else 'arr1是数组或字符串，arr2是字符串或数组
			If IsArray(s1) Then If Ubound(s1)<0 Or Err Then:Err.Clear:Exit Function:End If
			If Not IsArray(s1) Then If IsNull(s1) Or s1="" Then:Exit Function:End If
			If IsArray(s1) Then:a1 = s1
			If LCase(TypeName(s1))="string" Then:a1 = Split( AB.C.RP(s1, "|", ","),",")
			If LCase(TypeName(s2))="string" Then:s2 = AB.C.RP(s2, "|", ",")
			If IsArray(s2) Then:a2 = s2
			If LCase(TypeName(s2))="string" Then
				a3 = Split(s2,",")
				IF UBound(a1)<=UBound(a3) Then
					For i=0 To UBound(a1)
						x = a3(i) & ""
						y = x
						If Instr(x,":")>0 Then
							y = Split(x,":")(0)
							z = Split(x,":")(1)
							pt = AB.C.IIF(AB.C.isInstr("str,int,date",z),z,pt)
						End If
						Select Case pt&""
							Case "str" : RsObj(a1(i)&"") = y&""
							Case "int" : RsObj(a1(i)&"") = CLng(y)
							Case "date" : RsObj(a1(i)&"") = y&""
							Case Else : RsObj(a1(i)&"") = y&""
						End Select
					Next
				Else
					For i=0 To UBound(a3)
						x = a3(i) & ""
						y = x
						If Instr(x,":")>0 Then
							y = Split(x,":")(0)
							z = Split(x,":")(1)
							pt = AB.C.IIF(AB.C.isInstr("str,int,date",z),z,pt)
						End If
						Select Case pt&""
							Case "str" : RsObj(a1(i)&"") = y&""
							Case "int" : RsObj(a1(i)&"") = CLng(y)
							Case "date" : RsObj(a1(i)&"") = y&""
							Case Else : RsObj(a1(i)&"") = y&""
						End Select
					Next
					For i=UBound(a3)+1 To UBound(a1)
						RsObj(a1(i)&"") = ""
					Next
				End IF
			ElseIf AB.C.IsInt(s2) Or IsNull(s2) Then
				For i=0 To UBound(a1)
					RsObj(a1(i)&"") = s2
				Next
			Else
				IF UBound(a1)<=UBound(a2) Then
					For i=0 To UBound(a1)
						RsObj(a1(i)&"") = a2(i)
					Next
				Else
					For i=0 To UBound(a2)
						RsObj(a1(i)&"") = a2(i)
					Next
					For i=UBound(a2)+1 To UBound(a1)
						RsObj(a1(i)&"") = ""
					Next
				End IF
			End If
		End IF
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'************************************
	'@ 创建SQLCOMMAND对象
	'@ author:lajox; version:1.0.0 (2011-11-03)
	'************************************
	Public Function Cmd()
		On Error Resume Next
		Dim o_cmd
		Set o_cmd = Server.CreateObject("ADODB.Command")
		o_cmd.ActiveConnection = AB.db.Conn
		o_cmd.CommandType = 4
		o_cmd.Prepared = True
		Set Cmd = o_cmd
		Set o_cmd = Nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
    ' 方法名: AB.Dbo.CmdInsert
    ' 参  数: Table as Data Table
    ' 参  数: Params as Dictionary
    ' 作  用: 插入记录
	'******************************************************
	Public Function CmdInsert(Table,Params)
		On Error Resume Next
		Dim sqlCmd, sqlCmd_a, sqlCmd_b, parameteres, oParams
		Dim iName
		sqlCmd = "Set nocount on" & vbCrlf
		sqlCmd = sqlCmd & "Insert Into "&Table&" ("
		parameteres = " "
		Set oParams = CreateObject(AB.dictName)
		If Not IsNull(params) Then
			For Each iName in params
				sqlCmd_a = sqlCmd_a & iName & ","
				sqlCmd_b = sqlCmd_b & "@" & iName & ","
				parameteres = parameteres & "@" & iName & " varchar(8000)" & ","
			Next
		End If
		sqlCmd_a = Left(sqlCmd_a,Len(sqlCmd_a)-1)
		sqlCmd_b = Left(sqlCmd_b,Len(sqlCmd_b)-1)
		sqlCmd = sqlCmd & sqlCmd_a & ")  values(" & sqlCmd_b & ")"
		sqlCmd = sqlCmd & vbCrlf & "select Cast(IsNull(SCOPE_IDENTITY(),-100) as int)"
		parameteres = Left(parameteres,Len(parameteres)-1)
		oParams.Add "@stmt",sqlCmd
		oParams.Add "@parameters",parameteres
		If Not IsNull(Params) Then
			For Each iName in Params
				oParams.Add "@"&iName,Params(iName)&""
			Next
		End If
		CmdInsert = Me.ExecScalar("sp_executesql",oParams)
		Set oParams = Nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
    ' 方法名: AB.Dbo.CmdUpdate
    ' 参  数: Table as Data Table
    ' 参  数: Params as Dictionary
    ' 参  数: Where as 条件语句
    ' 作  用: 更新记录
	'******************************************************
	Public Function CmdUpdate(Table,Params,Where)
		On Error Resume Next
		Dim sqlCmd, parameteres, oParams
		Dim iName
		sqlCmd = "Set nocount on" & vbCrlf
		sqlCmd = sqlCmd & "Update "&Table&" set "
		parameteres = " "
		Set oParams = CreateObject(AB.dictName)
		If Not IsNull(params) Then
			For Each iName in params
				If InStr(iName,"#") > 0 Then
					params.Key(iName) = Replace(iName,"#","")
					iName = Replace(iName,"#","")
					sqlCmd = sqlCmd & iName & "=" & iName & " + @" & iName & ","
				Else
					sqlCmd = sqlCmd & iName & "=@" & iName & ","
				End If
				parameteres = parameteres & "@" & iName & " varchar(8000)" & ","
			Next
		End If
		sqlCmd = Left(sqlCmd,Len(sqlCmd)-1)
		If Trim(Where) <> "" Then sqlCmd=sqlCmd&" Where "&Where&""
		sqlCmd = sqlCmd & vbCrlf & "select CAST(IsNull(@@ROWCOUNT,-100) as int)"
		parameteres = Left(parameteres,Len(parameteres)-1)
		oParams.Add "@stmt",sqlCmd
		oParams.Add "@parameters",parameteres
		If Not IsNull(Params) Then
			For Each iName in Params
				oParams.Add "@"&iName,Params(iName)&""
			Next
		End If
		CmdUpdate = Me.ExecScalar("sp_executesql",oParams)
		Set oParams = Nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
	'@ 功能：执行存储过程并返回记录集
	'******************************************************
	Public Function ExecRecordSet(ByVal commandName , ByVal params)
		On Error Resume Next
		Set ExecRecordSet = ExecSqlCommand(commandName , params , 2)
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
	'@ 功能：执行存储过程并返回记录集第一行第一列
	'******************************************************
	Public Function ExecScalar(ByVal commandName , ByVal params)
		On Error Resume Next
		Dim rs : Set rs = ExecSqlCommand(commandName , params , 2)
		If Not rs.EOF And Not rs.BOF Then
			ExecScalar = rs(0).Value
		Else
			ExecScalar = NULL
		End If
		rs.Close
		Set rs = Nothing
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
	'@ 功能：执行存储过程并返回一个值
	'******************************************************
	Public Function ExecReturn(ByVal commandName , ByVal params)
		On Error Resume Next
		ExecReturn = ExecSqlCommand(commandName , params , 1)
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
	'@ 功能：执行存储过程不返回任何内容
	'******************************************************
	Public Function ExecNonQuery(ByVal commandName , ByVal params)
		On Error Resume Next
		ExecNonQuery = ExecSqlCommand(commandName , params , 0)
		ErrDo '出错处理
		On Error Goto 0
	End Function

	'******************************************************
	'@ 功能：执行存储过程并返回记录集和一个值
	'******************************************************
	Public Function ExecRsValue(ByVal commandName , ByVal params)
		On Error Resume Next
		ExecRsValue = ExecSqlCommand(commandName , params , 3)
		ErrDo '出错处理
		On Error Goto 0
	End Function

    '开始事务
	Public Sub BeginTrans()
		AB.db.Conn.BeginTrans()
	End Sub

    '回滚事务
	Public Sub RollBackTrans()
		AB.db.Conn.RollBackTrans()
	End Sub

    '提交事务
	Public Sub CommitTrans()
		AB.db.Conn.CommitTrans()
	End Sub

	'********************************************************************
	'@ 功能：执行存储过程
	'********************************************************************
	' commandName		存储过程名称
	' params			参数集合，必须使用 Scripting.Dictionary 对象定义
	' returnMode		返回模式
	'					0	不返回任何参数或对象
	'					1	执行后得到返回值
	'					2	执行后得到记录集
	'					3	执行后得到返回值和记录集
	'********************************************************************
	Private Function ExecSqlCommand(ByVal commandName , ByVal params , ByVal returnMode)
		Dim cmd : Set cmd = Server.CreateObject("ADODB.Command")
		Dim iName : iName = ""
		Dim RSReturn : Set RSReturn = Nothing
		DIM RSStream : SET RSStream	= Server.CreateObject(AB.SteamName)
		Dim ReturnValue : ReturnValue = ""
		cmd.ActiveConnection = Me.conn
		cmd.CommandText = commandName
		cmd.CommandType = 4
		cmd.NamedParameters = True
		cmd.Prepared = True
		If returnMode = 1 Or returnMode = 3 Then
			cmd.Parameters.Append cmd.CreateParameter("@ReturnValue", 2, 4)
		End If
		If Not IsNull(params) Then
			For Each iName in params
				If iName <> "@stmt" And iName <> "@statement" And iName <> "@parameters" Then
					If Len(params(iName)) < 4000 Or IsNumeric(params(iName)) Then
						cmd.Parameters.Append cmd.CreateParameter(iName , 202, 1, 4000, params(iName)&"")
					Else
						cmd.Parameters.Append cmd.CreateParameter(iName , 203, 1, Len(params(iName)) + 2, params(iName)&"")
					End If
				Else
					cmd.Parameters.Append cmd.CreateParameter(iName , 202, 1, 4000, params(iName))
				End If
			Next
		End If
		Select Case returnMode
			' 执行后得到返回值
			Case 1
				Call cmd.Execute(, , 128)
				ExecSqlCommand = cmd("@ReturnValue").Value
				' 执行后得到记录集
			Case 2
				Set ExecSqlCommand = cmd.Execute()
			Case 3
				Set RSReturn = cmd.Execute()
				Call RSReturn.Save(RSStream,1)
				RSReturn.Close
				Call RSReturn.Open(RSStream)
				ExecSqlCommand = Array(RSReturn, cmd("@ReturnValue").Value)
				' 默认方式，不返回任何参数或对象
			Case Else
				Call cmd.Execute(ExecSqlCommand, , 128)
		End Select
		Set cmd = Nothing
	End Function

	Private Function ErrDo
		IF Err.Number<>0 Then
			errid=Err.Number:errdes=Err.Description:Err.Clear
			On Error Goto 0
			If AB.db.Debug Then
				AB.Error.Show errid,errdes
			End If
		End IF
		On Error Goto 0
	End Function

End Class
%>
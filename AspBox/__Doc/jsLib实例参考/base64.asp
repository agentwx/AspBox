<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "jsLib"
Dim jsLib : Set jsLib = AB.jsLib.New
jsLib.Inc("base64.js")
Dim jso : Set jso = jsLib.Object
Dim str : str = ""

Dim text : text = "hello,kitty!你好" '要加密的原字符串
Dim entext : entext = jso.base64encode(text) '加密
Dim otext : otext = jso.base64decode(entext) '解密(还原)

ab.c.printcn entext '@return: aGVsbG8lMkNraXR0eSUyMSV1NEY2MCV1NTk3RA==
ab.c.printcn otext '@return: hello,kitty!你好
%>
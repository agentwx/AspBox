
======================================================================================

1.使用无惧上传类举例说明：

======================================================================================

【main.asp】代码如：

<form name="form1" method="post" action="upload.asp" enctype="multipart/form-data">
<input type=file name="img1"><br>
<input type=submit name="submit" value="提交">
</form>

'-------------------------------------------------------------------------------

【upload.asp】代码如：

<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
dim upfile
ab.use "up" : ab.up.use "upfile"
'Set upfile = new cls_ab_up_upfile
Set upfile = ab.up.upfile

upfile.AllowExt="jpg;jpeg;gif;png;" '设置上传类型的白名单
'取得上传数据,限制最大上传10M 计算方法为 10240000/1000000=10.24M
upfile.GetData(10240000)

'判决是否出错
if upfile.isErr then
  select case upfile.err
    case 1
      Response.Write "你没有上传数据"
    case 2
      Response.Write "上传的文件超出限制,最大10M"
  end select
else
  '执行保存文件代码
  dim oFile,SavePath,SaveFilename
  SavePath = "c:\" '文件保存路径
  set oFile=upfile.file("img1")
  upfile.SaveToFile "img1", SavePath & oFile.filename
  'SaveFilename = upfile.AutoSave("img1",SavePath) '执行自动保存文件代码，SaveFilename为保存的文件名(自动重命名)
  if upfile.iserr then 
	Response.Write upfile.errmessage & "<br>"
  else
	Response.Write "上传成功，路径："& SavePath & oFile.FileName &"<br>"
	'Response.Write "上传成功，路径："& SavePath & SaveFilename &"<br>"
  end if
end if
set upfile=nothing
%>

======================================================================================

2.使用化境HTTP上传类举例说明：

======================================================================================

【main.asp】代码如：

<% dim uppath : uppath = "./upload" %>
<form name="uploadform" method="post"
 action="upload.asp?action=SaveUploadFile&filepath=<%=uppath%>" enctype="multipart/form-data">
上传文件<br>
<input type="file" name="file1"><br>
<input type="file" name="file2"><br>
<input type="file" name="file3"><br>
<input type="submit" name="Submit" value="确定上传" class=""/>
<input type="hidden" name="filepath" value="<%=uppath%>" size="60">
</form>

'-------------------------------------------------------------------------------

【upload.asp】代码如：

<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
If Request.QueryString("action") = "SaveUploadFile" then
	Call SaveUploadFile '保存上传
End If

Function SaveUploadFile
	On Error Resume Next '防错
	Response.Buffer = True
	Randomize
	Dim RanNum,Fso
	RanNum = Int(90000*rnd)+10000
	Set Fso = Server.CreateObject("Scripting.FileSystemObject")
	Const filetype = ".bmp.gif.jpg.png.rar.zip.7z" '允许上传的文件类型
    Const MaxSize = 10240000 '允许的文件大小 10M
    Dim upload,oFile,formName,formPath,oldfilename,newfilename,temp,fileExt
	ab.use "up" : ab.up.use "u5x"
	'set upload = new cls_ab_up_u5x
	set upload = ab.up.u5x
	upload.init() '起始执行函数,必须提前调用!!
    If upload.Form("filepath")<>"" Then
		formPath = Server.mappath(upload.Form("filepath"))
		If Right(formPath, 1)<>"\" Then formPath = formPath&"\"
		If fso.FolderExists(formPath)<>true Then
			fso.CreateFolder(formPath)
		End If
		Dim isUploaded : isUploaded = True '是否上传成功
		For Each formName in upload.objFile
			Dim iUploaded : iUploaded = True
			Set oFile = upload.File(formName)
			If oFile.FileName="" Then
				iUploaded = True
			Else
				temp = Split(oFile.FileName, ".")
				fileExt = temp(UBound(temp))
				If InStr(1, filetype, LCase(fileExt))>0 Then
					oldfilename = Split(oFile.FileName,"\")(UBound(Split(oFile.FileName,"\"))) '原名
					newfilename = RanNum&"."&Split(oldfilename,".")(UBound(Split(oldfilename,"."))) '随机生成文件名
					If oFile.FileSize>0 And (oFile.FileSize<MaxSize) Then
						iUploaded = (iUploaded and oFile.SaveAs(formPath&oldfilename))
						If Err<>0 Then
							iUploaded = False
							Response.Write "<script>alert(""上传过程出错！"");history.go(-1);</script>"
							Err.Clear
							Response.End
						End If
					Else
						iUploaded = False
					End If
				Else
					iUploaded = False
				End If
			End If
			isUploaded = isUploaded and iUploaded
			Set oFile = Nothing
		Next
		IF isUploaded=True Then
			Response.Write("<script>alert(""上传成功！"");history.go(-1);</script>")
		Else
			Response.Write("<script>alert(""上传失败！"");history.go(-1);</script>")
		End IF
	Else
		Response.Write("<script>history.go(-1);</script>")
	End If
    Set upload = Nothing
End Function
%>

======================================================================================

3.使用艾恩无组件举例说明：

======================================================================================

【form.html】代码如：

单文件上传<br />
<form action="upload.asp" method="post" enctype="multipart/form-data">
文件 <input type="file" name="file1" /> <input type="submit" value="上传" />
</form>

'-------------------------------------------------------------------------------

【upload.asp】代码如：

<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
Dim Upload
'---------------------------
'创建类实例
ab.use "up" : ab.up.use "an"
'Set Upload = New Cls_AB_Up_An
Set Upload = AB.Up.An

'设置单个文件最大上传限制,按字节计；默认为不限制
Upload.SingleSize = 2 * 1024 * 1024 '2M

'设置最大上传限制,按字节计；默认为不限制
Upload.MaxSize = 2 * 1024 * 1024 '2M

'设置合法扩展名,以|分割,忽略大小写,如"jpg|bmp|jpeg|gif|png",若不限制则为"*"
Upload.Exe = "*"

'设置文本编码，默认为utf-8
Upload.Charset="utf-8"

'获取并保存数据,必须调用本方法
Upload.GetData()
'---------------------------
if upload.ErrorID>0 then
	response.Write upload.Description
else
	dim file,savepath,frm,result,msg
	savepath = "upload"
	for each frm in upload.forms("-1")
		response.Write frm & "=" & upload.forms(frm) & "<br />"
	next
	set file = upload.files("file1")
	if not(file is nothing) then
		result = file.saveToFile(savepath,0,true)
		if result then
			response.Write "文件'" & file.LocalName & "'上传成功，保存位置'" & server.MapPath(savepath & "/" & file.filename) & "',文件大小" & file.size & "字节"
		else
			response.Write file.Exception
		end if
	end if
end if
set upload = nothing
%>
<%
'######################################################################
'## ctrl.js.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Js Control Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/01/05 9:43
'## Description :   AspBox Mvc Js Control Block(MVC的脚本Js控制模块)
'######################################################################

Class Cls_Ctrl_Js

	Private i

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'******************************
	'函数名：Redirect
	'作  用：页面跳转
	'******************************
	Sub Redirect(Byval url)
		AB.C.RR url
		Response.End
	End Sub

	'******************************
	'函数名：GoReferer
	'作  用：返回上页
	'******************************
	Sub GoReferer()
		Redirect Request.ServerVariables("HTTP_REFERER")
		Response.End
	End Sub

	'**************************************************************************************
	'函数名：AlertGo
	'作  用：消息框
	'举  例：ab.use "mvc" : ctrl.use "js" : ctrl.js.alertgo "确认删除？","do.asp?action=del?id=12"
	'**************************************************************************************
	Sub AlertGo(Byval msg,Byval url)
		AB.C.Put "<script type=""text/javascript"">alert("""&msg&""");location.replace("""&url&""");</script>"
	End Sub

	'**************************************************************************************
	'函数名：AlertBack
	'参  数：msgstr	-- 弹出信息
	'作  用：消息框
	'举  例：ab.use "mvc" : ctrl.use "js" : ctrl.js.alertback "操作成功！"
	'**************************************************************************************
	Sub alertBack(Byval msg)
		AB.C.Put "<script type=""text/javascript"">alert(""" & msg & """);history.back();</script>"
	End Sub

	'**************************************************************************************
	'函数名：AlertClose
	'参  数：msgstr	-- 弹出信息
	'作  用：消息框后关闭窗口
	'举  例：ab.use "mvc" : ctrl.use "js" : ctrl.js.alertclose "操作成功！"
	'**************************************************************************************
	Sub alertClose(Byval msg)
		AB.C.Put "<script type=""text/javascript"">alert(""" & msg & """);window.close();</script>"
	End Sub

End Class
%>
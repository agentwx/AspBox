<%
'######################################################################
'## util.ubb.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc UBB-Util Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/04/29 21:51
'## Description :   AspBox Mvc UBB-Util Block(UBB工具拓展模块)
'######################################################################

Class Cls_Util_UBB

	Private s_mode

    Private Sub Class_Initialize()
        s_mode = 0
    End Sub

	Private Sub Class_Terminate()

	End Sub

	'@******************************************************************************
	'@ 属性：0、1、2、3几个等级（1：支持文字样式；2：支持多媒体；3：支持动画脚本）
	'@******************************************************************************

	Public Property Let Mode(ByVal s)
		s_mode = s
	End Property

	Public Property Get Mode()
		Mode = s_mode
	End Property

	'@ *****************************************************************************
	'@ 过程名:  Util.UBB.E(s) {简写为： Mvc.Util.UBB(s) }
	'@ 返  回:  String (字符串)
	'@ 作  用:  UBB代码解析器（对UBB代码进行解析）,对UBB代码解析还原
	'==Param========================================================================
	'@ s 	: 字符串 [String]
	'==DEMO=========================================================================
	'@ AB.use "Mvc" : Util.use "UBB" : AB.C.Print Util.UBB("[URL]http://www.baidu.com/[/URL]")
	'@ => <a href="http://www.baidu.com/" target="_blank">http://www.baidu.com/</a>
	'@ *****************************************************************************

	Public Default Function E(ByVal s)
		E = Encode(s)
	End Function

	Private Function Encode(ByVal s)
		If Len(s)>0 Then
			s = Replace(s, " ", "&nbsp;")
		Else
			Exit Function
		End If
		Dim re, i
		Set re = New regexp
		re.IgnoreCase = true
		re.Global = True
		re.Pattern = "(js):"
		s = re.Replace(s, "j#115;:")
		re.Pattern = "(vbs):"
		s = re.Replace(s, "vb&#115;:")
		re.Pattern = "(script)"
		s = re.Replace(s, "&#115cript")
		re.Pattern = "(value)"
		s = re.Replace(s, "&#118alue")
		re.Pattern = "(document.cookie)"
		s = re.Replace(s, "documents&#46cookie")
		re.Pattern = "(on(mouse|exit|error|click|key))"
		s = re.Replace(s, "&#111n$2")
		re.Pattern = "(\[img\])(\S+?)(\[\/img\])"
		s = re.Replace(s, "<img src=""$2"" alt="""">")
		re.Pattern = "(\[URL\])(\S+?)(\[\/URL\])"
		s = re.Replace(s, "<a href=""$2"" target=""_blank"">$2</a>")
		re.Pattern = "(\[URL=(\S+?)\])(.+?)(\[\/URL\])"
		s = re.Replace(s, "<a href=""$2"" target=""_blank"">$3</a>")
		re.Pattern = "(\[email\])(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+){0,100})(\[\/email\])"
		s = re.Replace(s, "<a href=""mailto:$2"">$2</a>")
		re.Pattern = "(\[email=(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+){0,80})\])(.[^\[]*)(\[\/email\])"
		s = re.Replace(s, "<a href=""mailto:$2"" target=""_blank"">$6</a>")
		If s_mode >= 1 Then
			re.Pattern = "(\[b\])((.|\n){0,}?)(\[\/b\])"
			s = re.Replace(s, "<strong>$2</strong>")
			re.Pattern = "(\[i\])((.|\n){0,}?)(\[\/i\])"
			s = re.Replace(s, "<i>$2</i>")
			re.Pattern = "(\[u\])((.|\n){0,}?)(\[\/u\])"
			s = re.Replace(s, "<u>$2</u>")
			re.Pattern = "(\[code\])((.|\n){0,}?)(\[\/code\])"
			s = re.Replace(s, "<code class=""ubb""><p>$2</p></code>")
			re.Pattern = "(\[size=(\d+)\])((.|\n){0,}?)(\[\/size\])"
			s = re.Replace(s, "<font size=""$2"">$3</span>")
			re.Pattern = "\[h(\d+)\]((.|\n){0,}?)(\[\/h(\d+)\])"
			s = re.Replace(s, "<h$1>$2</h$1>")
			re.Pattern = "\[align=(center|left|right)\]((.|\n){0,}?)(\[\/align\])"
			s = re.Replace(s, "<div style=""display:block;text-align:$1;"">$2</div>")
			re.Pattern = "(\[quote\])"
			s = re.Replace(s, "<blockquote class=""ubb"">")
			re.Pattern = "(\[\/quote\])"
			s = re.Replace(s, "</blockquote>")
		End If
		If s_mode >= 2 Then
			re.Pattern = "\[(rm|mp|qt)=.+?\]"
			s = re.Replace(s, "[media]")
			re.Pattern = "\[\/(rm|mp|qt)=.+?\]"
			s = re.Replace(s, "[/media]")
			re.Pattern = "\[(dir|flash)=.+?\]"
			s = re.Replace(s, "[swf]")
			re.Pattern = "\[\/(dir|flash)=.+?\]"
			s = re.Replace(s, "[/swf]")
			re.Pattern = "(\[swf\])((http|https|ftp):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\+\-~`@\':!%#]|(&amp;)|&)+\.swf)(\[\/swf\])"
			s = re.Replace(s, "<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"" width=""352"" height=""288"" align=""middle""><param name=""movie"" value=""$2"" /><param name=""quality"" value=""high""><param name=""menu"" value=""false""><embed src=""$2"" quality=""high"" width=""352"" height=""288"" align=""middle"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" /></object>")

			re.Pattern = "(\[flash\])((http|https|ftp):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\+\-~`@\':!%#]|(&amp;)|&)+\.swf)(\[\/flash\])"
			s = re.Replace(s, "<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"" width=""352"" height=""288"" align=""middle""><param name=""movie"" value=""$2"" /><param name=""quality"" value=""high""><param name=""menu"" value=""false""><embed src=""$2"" quality=""high"" width=""352"" height=""288"" align=""middle"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer"" /></object>")
			re.Pattern = "\[media\]((http|ftp|mms|https):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\+\-~`@\':!%#]|(&amp;)|&)+\.(wmv|asf|wm|wma|wmv|wmx|wmd|avi|mpeg|mpg|mpa|mpe|dat|w1v|mp2|asx))\[\/media]"
			s = re.Replace(s, "<object><embed src=""$1"" autostart=""false"" playcount=""1""/></object>")

			re.Pattern = "\[media\]((http|ftp|mms|https):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\+\-~`@\':!%#]|(&amp;)|&)+\.(mp3|mid))\[\/media]"
			s = re.Replace(s, "<object classid=""CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"" width=""352"" height=""45""><param name=""filename"" value=""$1""/><embed src=""$1"" playcount=""1""></embed></object>")

			re.Pattern = "\[media\]((http|ftp|rtsp|https):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\+\-~`@\':!%#]|(&amp;)|&)+\.(ra|rm|rmj|rms|mnd|ram|rmm|r1m|rom|mns))\[\/media]"
			s = re.Replace(s, "<object classid=clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa width=""352"" height=""288""><param name=""src"" value=""$1""/><param name=""console"" value=""clip1""/><param name=""controls"" value=""imagewindow""/><param name=""autostart"" value=""false""/></object><object classid=""clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa"" height=""32"" width=""352""><param name=""src"" value=""$1""/><param name=""controls"" value=""controlpanel""/><param name=""console"" value=""clip1""/></object>")
		End If
		If s_mode >= 3 Then
			re.Pattern = "(\[fly\])((.|\n){0,}?)(\[\/fly\])"
			s = re.Replace(s, "<marquee width=""100%"" behavior=""alternate"" scrollamount=""3"">$2</marquee>")
			re.Pattern = "(\[move\])((.|\n){0,}?)(\[\/move\])"
			s = re.Replace(s, "<marquee scrollamount=""3"">$2</marquee>")
			re.Pattern = "(\[color=(.{3,10})\])((.|\n){0,}?)(\[\/color\])"
			s = re.Replace(s, "<span style=""color:$2;"">$3</span>")
			re.Pattern = "(\[glow=.+?\])((.|\n)+?)(\[\/glow\])"
			s = re.Replace(s, "$2")
			re.Pattern = "(\[shadow=.+?\])((.|\n)+?)(\[\/shadow\])"
			s = re.Replace(s, "$2")
		End If
		If InStr(LCase(s), "http://")>0 Then
			're.Pattern = "(^|[^<=""'])(http:(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\=\?\+\-~`@\':!%#]|(&amp;)|&)+)"
			's = re.Replace(s, "$1<a target=""_blank"" href=""$2"">$2</a>")
		End If
		'识别www等开头的网址
		If InStr(LCase(s), "www.")>0 Then
			re.Pattern = "(^|[^\/\\\w<=""])((www)\.(\w)+\.([\w\/\\\.\=\?\+\-~`@\'!%#]|(&amp;))+)"
			s = re.Replace(s, "$1<a target=""_blank"" href=""http://$2"">$2</a>")
		End If
		s = Replace(s, Chr(13)&Chr(10), "<br />")
		Set re = Nothing
		Encode = s
	End Function

	Function ubbcode(ByVal str, ByVal spopedom)
	  Dim re, i
	  ubbcode = str
	  If (InStr(str, "[") = 0 Or InStr(str, "]") = 0) And InStr(str, "http://") = 0 Then
		Exit Function
	  End If
	  Set re = New regexp
	  re.ignorecase = True
	  re.Global = True
	  If InStr(1, str, "[img]", 1) > 0 Then
		re.Pattern = "(\[img\])(.[^\[]*)(\[\/img\])"
		str = re.Replace(str, "<a href=""$2"" target=""_blank""><img src=""$2"" border=""0"" alt=""$2"" onload=""iresize(this,1,500)""></a>")
	  End If
	  If InStr(1, str, "[/dir]", 1) > 0 Then
		re.Pattern = "\[dir=*([0-9]*),*([0-9]*)\](.[^\[]*)\[\/dir]"
		str = re.Replace(str, "<object classid=clsid:166b1bca-3f9c-11cf-8075-444553540000 codebase=http://download.macromedia.com/pub/shockwave/cabs/director/sw.cab#version=7,0,2,0 width=$1 height=$2><param name=src value=$3><embed src=$3 pluginspage=http://www.macromedia.com/shockwave/download/ width=$1 height=$2></embed></object>")
	  End If
	  If InStr(1, str, "[/qt]", 1) > 0 Then
		re.Pattern = "\[qt=*([0-9]*),*([0-9]*)\](.[^\[]*)\[\/qt]"
		str = re.Replace(str, "<embed src=$3 width=$1 height=$2 autoplay=true loop=false controller=true playeveryframe=false cache=false scale=tofit bgcolor=#000000 kioskmode=false targetcache=false pluginspage=http://www.apple.com/quicktime/>")
	  End If
	  If InStr(1, str, "[/mp]", 1) > 0 Then
		re.Pattern = "\[mp=*([0-9]*),*([0-9]*)\](.[^\[]*)\[\/mp]"
		str = re.Replace(str, "<object align=middle classid=clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95 class=object id=mediaplayer width=$1 height=$2 ><param name=showstatusbar value=-1><param name=filename value=$3><embed type=application/x-oleobject codebase=http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#version=5,1,52,701 flename=mp src=$3  width=$1 height=$2></embed></object>")
	  End If
	  If spopedom = 1 Then
		If InStr(1, str, "[/rm]", 1) > 0 Then
		  re.Pattern = "\[rm=*([0-9]*),*([0-9]*)\](.[^\[]*)\[\/rm]"
		  str = re.Replace(str, "<object classid=clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa class=object id=raocx width=$1 height=$2><param name=src value=$3><param name=console value=clip1><param name=controls value=imagewindow><param name=autostart value=true></object><br><object classid=clsid:cfcdaa03-8be4-11cf-b84b-0020afbbccfa height=32 id=video2 width=$1><param name=src value=$3><param name=autostart value=-1><param name=controls value=controlpanel><param name=console value=clip1></object>")
		End If
		If InStr(1, str, "[/flash]", 1) > 0 Then
		  re.Pattern = "\[flash=*([0-9]*),*([0-9]*)\](.[^\[]*)\[\/flash\]"
		  str = re.Replace(str, "<script type=""text/javascript"">writeFlashHTML2(""_version=8,0,0,0"" ,""_swf=$3"", ""_width=$1"", ""_height=$2"", ""_quality=high"");</script>")
		End If
	  End If
	  If InStr(1, str, "[/url]", 1) > 0 Then
		re.Pattern = "(\[url\])(.[^\[]*)(\[\/url\])"
		str = re.Replace(str, "<a href=""$2"" target=_blank>$2</a>")
		re.Pattern = "(\[url=(.[^\]]*)\])(.[^\[]*)(\[\/url\])"
		str = re.Replace(str, "<a href=""$2"" target=_blank>$3</a>")
	  End If
	  If InStr(1, str, "[/email]", 1) > 0 Then
		re.Pattern = "(\[email\])(.[^\[]*)(\[\/email\])"
		str = re.Replace(str, "<a href=""mailto:$2"">$2</a>")
		re.Pattern = "(\[email=(.[^\[]*)\])(.[^\[]*)(\[\/email\])"
		str = re.Replace(str, "<a href=""mailto:$2"">$3</a>")
	  End If
	  If InStr(1, str, "http://", 1) > 0 Then
		re.Pattern = "^(http://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "(http://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)$"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "([^>=""])(http://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "$1<a target=_blank href=$2>$2</a>")
	  End If
	  If InStr(1, str, "ftp://", 1) > 0 Then
		re.Pattern = "^(ftp://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "(ftp://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)$"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "([^>=""])(ftp://[a-za-z0-9\.\/=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "$1<a target=_blank href=$2>$2</a>")
	  End If
	  If InStr(1, str, "rtsp://", 1) > 0 Then
		re.Pattern = "^(rtsp://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "(rtsp://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)$"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "([^>=""])(rtsp://[a-za-z0-9\.\/=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "$1<a target=_blank href=$2>$2</a>")
	  End If
	  If InStr(1, str, "mms://", 1) > 0 Then
		re.Pattern = "^(mms://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "(mms://[a-za-z0-9\./=\?%\-&_;~`@':+!]+)$"
		str = re.Replace(str, "<a target=_blank href=$1>$1</a>")
		re.Pattern = "([^>=""])(mms://[a-za-z0-9\.\/=\?%\-&_;~`@':+!]+)"
		str = re.Replace(str, "$1<a target=_blank href=$2>$2</a>")
	  End If
	  If InStr(1, str, "[/color]", 1) > 0 Then
		re.Pattern = "(\[color=(.[^\[]*)\])(.[^\[]*)(\[\/color\])"
		str = re.Replace(str, "<font color=$2>$3</font>")
	  End If
	  If InStr(1, str, "[/face]", 1) > 0 Then
		re.Pattern = "(\[face=(.[^\[]*)\])(.[^\[]*)(\[\/face\])"
		str = re.Replace(str, "<font face=$2>$3</font>")
	  End If
	  If InStr(1, str, "[/align]", 1) > 0 Then
		re.Pattern = "(\[align=(.[^\[]*)\])(.[^\[]*)(\[\/align\])"
		str = re.Replace(str, "<div align=$2>$3</div>")
	  End If
	  If InStr(1, str, "[/fly]", 1) > 0 Then
		re.Pattern = "(\[fly\])(.[^\[]*)(\[\/fly\])"
		str = re.Replace(str, "<marquee width=90% behavior=alternate scrollamount=3>$2</marquee>")
	  End If
	  If InStr(1, str, "[/move]", 1) > 0 Then
		re.Pattern = "(\[move\])(.[^\[]*)(\[\/move\])"
		str = re.Replace(str, "<marquee scrollamount=3>$2</marquee>")
	  End If
	  If InStr(1, str, "[/glow]", 1) > 0 Then
		re.Pattern = "\[glow=*([0-9]*),*(#*[a-z0-9]*),*([0-9]*)\](.[^\[]*)\[\/glow]"
		str = re.Replace(str, "<table width=$1 style=""filter:glow(color=$2, strength=$3)"">$4</table>")
	  End If
	  If InStr(1, str, "[/shadow]", 1) > 0 Then
		re.Pattern = "\[shadow=*([0-9]*),*(#*[a-z0-9]*),*([0-9]*)\](.[^\[]*)\[\/shadow]"
		str = re.Replace(str, "<table width=$1 style=""filter:shadow(color=$2, strength=$3)"">$4</table>")
	  End If
	  If InStr(1, str, "[/i]", 1) > 0 Then
		re.Pattern = "(\[i\])(.[^\[]*)(\[\/i\])"
		str = re.Replace(str, "<i>$2</i>")
	  End If
	  If InStr(1, str, "[/u]", 1) > 0 Then
		re.Pattern = "(\[u\])(.[^\[]*)(\[\/u\])"
		str = re.Replace(str, "<u>$2</u>")
	  End If
	  If InStr(1, str, "[/b]", 1) > 0 Then
		re.Pattern = "(\[b\])(.[^\[]*)(\[\/b\])"
		str = re.Replace(str, "<b>$2</b>")
	  End If
	  If InStr(1, str, "[/size]", 1) > 0 Then
		re.Pattern = "(\[size=1\])(.[^\[]*)(\[\/size\])"
		str = re.Replace(str, "<font size=1>$2</font>")
		re.Pattern = "(\[size=2\])(.[^\[]*)(\[\/size\])"
		str = re.Replace(str, "<font size=2>$2</font>")
		re.Pattern = "(\[size=3\])(.[^\[]*)(\[\/size\])"
		str = re.Replace(str, "<font size=3>$2</font>")
		re.Pattern = "(\[size=4\])(.[^\[]*)(\[\/size\])"
		str = re.Replace(str, "<font size=4>$2</font>")
	  End If
	  If InStr(1, str, "[/center]", 1) > 0 Then
		re.Pattern = "(\[center\])(.[^\[]*)(\[\/center\])"
		str = re.Replace(str, "<center>$2</center>")
	  End If
	  If InStr(1, str, "[/list]", 1) > 0 Then
		str = docode(str, "[list]", "[/list]", "<ul>", "</ul>")
		str = docode(str, "[list=1]", "[/list]", "<ol type=1>", "</ol id=1>")
		str = docode(str, "[list=a]", "[/list]", "<ol type=a>", "</ol id=a>")
	  End If
	  If InStr(1, str, "[/*]", 1) > 0 Then
		str = docode(str, "[*]", "[/*]", "<li>", "</li>")
	  End If
	  If InStr(1, str, "[/code]", 1) > 0 Then
		str = docode(str, "[code]", "[/code]", "<table cellpadding=""5"" cellspacing=""1"" border=""0"" width=""96%"" class=""quote"" align=""center""><tr><td class=""ash""><i>", "</i></td></tr></table>")
	  End If
	  If InStr(1, str, "[em]", 1) > 0 Then
		re.Pattern = "(\[em\])(.[^\[]*)(\[\/em\])"
		str = re.Replace(str, "<img src=""{$global.images}em/$2.gif"" border=""0"">")
	  End If
	  Dim isquote: isquote = True
	  Dim isqi
	  Do While isquote
		If InStr(1, str, "[/quote]", 1) > 0 Then
		  re.Pattern = "(\[quote\])(.[^\[]*)(\[\/quote\])"
		  str = re.Replace(str, "<table cellpadding=""5"" cellspacing=""1"" border=""0"" width=""96%"" class=""quote"" align=""center""><tr><td>$2</td></tr></table>")
		End If
		If isqi >= 10 Or InStr(1, str, "[/quote]", 1) = 0 Then isquote = False
		isqi = isqi + 1
	  Loop
	  Set re = Nothing
	  ubbcode = str
	End Function

	Function docode(ByVal fstring, ByVal fotag, ByVal fctag, ByVal frotag, ByVal frctag)
	  Dim fotagpos, fctagpos
	  fotagpos = InStr(1, fstring, fotag, 1)
	  fctagpos = InStr(1, fstring, fctag, 1)
	  While (fctagpos > 0 And fotagpos > 0)
		fstring = Replace(fstring, fotag, frotag, 1, 1, 1)
		fstring = Replace(fstring, fctag, frctag, 1, 1, 1)
		fotagpos = InStr(1, fstring, fotag, 1)
		fctagpos = InStr(1, fstring, fctag, 1)
	  Wend
	  docode = fstring
	End Function

	'@ *****************************************************************************
	'@ 过程名:  Mvc.Util.UBB.HtmlToUBB(s)
	'@ 返  回:  String (字符串)
	'@ 作  用:  HTML转UBB
	'==Param========================================================================
	'@ s 	: 字符串 [String]
	'==DEMO=========================================================================
	'@ AB.use "Mvc" : Util.use "UBB" : AB.C.Print Util.UBB.HtmlToUBB("<img src='http://www.baidu.com/logo.gif'>")
	'@ AB.use "Mvc" : Util.use "UBB" : AB.C.Print Util.UBB.HtmlToUBB("<a href='http://www.baidu.com/'>百度</a>")
	'@ *****************************************************************************

	Function HtmlToUBB(ByVal str)
		Dim s : s = str
		If IsNull(s) Or s="" Then : HtmlToUBB = "" : Exit Function : End If
		s = replace(s, chr(0), "")
		's = replace(s, chr(13), "")
		Dim re : Set re = New RegExp
		re.IgnoreCase = True
		re.Global = True
		re.Pattern = "([\f\n\r\t\v])":s = re.replace(s,"")
		re.Pattern = "(on(load|click|dbclick|mouseover|mousedown|mouseup|mousewheel|keydown)=(""|')[^\2]*\2)":s = re.replace(s,"")
		re.Pattern = "(<script[^>]*?>([\w\W]*?)<\/script>)":s = re.replace(s, "")
		re.pattern = "(<font[^>]+color=(""|'|)([^\2]+?[^\s]*)\2[^>]*>(.*?)<\/font>)":s = re.replace(s,"[color=$3]$4[/color]")
		re.Pattern = "(<font[^>]+color=([^ >]+)[^>]*>(.*?)<\/font>)":s = re.replace(s,"[color=$2]$3[/color]")
		re.pattern = "(<a[^>]+href=(""|'|)([^\2]+?[^\s]*)\2[^>]*>(.*?)<\/a>)":s = re.replace(s,"[url=$3]$3[/url]")
		re.pattern = "(<img[^>]+src=(""|'|)([^\2]*?[^\s]*)\2[^>]*>)":s = re.replace(s,"[img]$3[/img]")
		re.pattern = "(<b([^>]*)>(((?!<b[^>]*>|<\/b>)[\s\S])*)<\/b>)":s = re.replace(s,"[b]$3[/b]")
		re.pattern = "(<strong([^>]*)>(((?!<strong[^>]*>|<\/strong>)[\s\S])*)<\/strong>)":s = re.replace(s,"[b]$3[/b]")
		re.pattern = "(<u([^>]*)>(((?!<u[^>]*>|<\/u>)[\s\S])*)<\/u>)":s = re.replace(s,"[u]$3[/u]")
		re.pattern = "(<i([^>]*)>(((?!<i[^>]*>|<\/i>)[\s\S])*)<\/i>)":s = re.replace(s,"[i]$3[/i]")
		re.Pattern = "(<p[^>]+align=""([^"">]+)""[^>]*>(.*?)<\/p>)":s = re.replace(s, "[align=$2]$3[/align]")
		re.Pattern = "(<p[^>]+align=([^"">]+)[^>]*>(.*?)<\/p>)":s = re.replace(s, "[align=$2]$3[/align]")
		re.Pattern = "(<([\/]?)p>)":s = re.replace(s, chr(10))
		re.Pattern = "(&nbsp;)":s = re.replace(s, " ")
		re.Pattern = "(&amp;)":s = re.replace(s, "&")
		re.Pattern = "(&quot;)":s = re.replace(s, """")
		re.Pattern = "(&#39;)":s = re.replace(s, "'")
		re.Pattern = "(&#123;)":s = re.replace(s,"{")
		re.Pattern = "(&#125;)":s = re.replace(s,"}")
		re.Pattern = "(&#36;)":s = re.replace(s,"$")
		re.Pattern = "(<br[^>]*?>)":s = re.replace(s, Chr(10))
		re.Pattern = "((<p>&nbsp;</p>)|(<p></p>))":s = re.replace(s, Chr(10))
		re.Pattern = "((<p[^>]*?>)|(</p>))":s = re.replace(s, Chr(10))
		re.Pattern = "(<[^>]*?>)":s = re.replace(s, "")
		re.Pattern = "(\[url=([^\]]+)\]\n(\[img\]\1\[\/img\])\n\[\/url\])":s = re.replace(s, "$2")
		're.Pattern = "(\n+)":s = re.replace(s, Chr(10))
		re.Pattern = "(&lt;)":s = re.replace(s, "<")
		re.Pattern = "(&gt;)":s = re.replace(s, ">")
		Set re = Nothing
		HtmlToUBB = s
	End Function

	'@ *****************************************************************************
	'@ 过程名:  Util.UBB.closeHtml(s)
	'@ 返  回:  String (字符串)
	'@ 作  用:  自动闭合html
	'@ 			目前支持闭合的标签有:
	'@ 			 p,DIV,span,table,ul,font,b,u,i,h1,h2,h3,h4,h5,h6
	'==Param========================================================================
	'@ s 	: 字符串 [String]
	'==DEMO=========================================================================
	'@ AB.use "Mvc" : Util.use "UBB" : AB.C.Print Util.UBB.closeHtml("<span><a href='#'>我</a>")
	'@ *****************************************************************************

	Function closeHtml(Byval s)
		Dim str, Tags, arrTags, i, OpenPos, ClosePos, re, strMatchs, j, Match : str = s
		Set re = New RegExp
		re.IgnoreCase = True
		re.Global = True
		Tags = "p,div,span,table,ul,font,b,u,i,h1,h2,h3,h4,h5,h6" '设置需要闭合的标签
		arrTags = Split(Tags,",")
		For i = 0 To UBound(arrTags)
			OpenPos = 0 '标签开始标记个数
			ClosePos = 0 '标签结束标记个数
			re.Pattern = "<(" + trim(arrTags(i)) + ")((\s+[^<>])+|)>"
			Set strMatchs = re.Execute(str)
			For Each Match in strMatchs
				OpenPos = OpenPos + 1
				str = Replace(str, Match.Value, "<"& LCase(Match.SubMatches(0)) & Match.SubMatches(1) &">") '转为小写
			Next
			re.Pattern = "<\/" + arrTags(i) + ">"
			Set strMatchs = re.Execute(str)
			For Each Match in strMatchs
				ClosePos = ClosePos + 1
				str = Replace(str, Match.Value, "</"& LCase(Match.SubMatches(0)) &">") '转为小写
			Next
			For j = 1 To OpenPos - ClosePos '当开始与结束标记数量不一致时，闭合当前标签
				str = str + "</" + arrTags(i) + ">"
			Next
		Next
		closeHtml = str
		Set re = Nothing
	End Function

	'@ *****************************************************************************
	'@ 过程名:  Util.UBB.closeUBB(s)
	'@ 返  回:  String (字符串)
	'@ 作  用:  自动闭合UBB
	'==Param========================================================================
	'@ s 	: 字符串 [String]
	'==DEMO=========================================================================
	'@ AB.use "Mvc" : Util.use "UBB" : AB.C.Print Util.UBB.closeUBB("[B][I]aaa[/I]")
	'@ *****************************************************************************

	Function closeUBB(Byval s)
		Dim str, Tags, arrTags, i, OpenPos, ClosePos, re, strMatchs, j, Match : str = s
		Set re = New RegExp
		re.IgnoreCase = True
		re.Global = True
		Tags = "code,quote,list,color,align,font,size,b,i,u,html"
		arrTags = Split(Tags,",")
		For i = 0 To UBound(arrTags)
			OpenPos = 0
			ClosePos = 0
			re.Pattern = "\[(" + trim(arrTags(i)) + ")(=[^\[\]]+|)\]"
			Set strMatchs = re.Execute(str)
			For Each Match in strMatchs
				OpenPos = OpenPos + 1
				str = Replace(str, Match.Value, "["& LCase(Match.SubMatches(0)) & Match.SubMatches(1) &"]")
			Next
			re.Pattern = "\[\/(" + arrTags(i) + ")]"
			Set strMatchs = re.Execute(str)
			For Each Match in strMatchs
				ClosePos = ClosePos + 1
				str = Replace(str, Match.Value, "[/"& LCase(Match.SubMatches(0)) &"]")
			Next
			For j = 1 To OpenPos - ClosePos
				str = str + "[/" + Lcase(arrTags(i)) + "]"
			Next
		Next
		closeUBB = str
		Set re = Nothing
	End Function

	Function TrimNewline(ByVal strContent)
		If IsNull(strContent) Then : TrimNewline = "" : Exit Function : End If
		strContent = Replace(strContent, Chr(13), "")
		If strContent = "" Then : TrimNewline = "" : Exit Function : End If
		If Left(strContent,1) = Chr(10) Then
			strContent = Replace(strContent, Chr(10), vbNullString, 1, 1)
			strContent = TrimNewline(strContent)
		End If
		If Right(strContent,1) = Chr(10) Then
			strContent = Left(strContent,Len(strContent)-1)
			strContent = TrimNewline(strContent)
		End If
		'strContent = Replace(strContent, Chr(10), vbCrLf)
		TrimNewline = strContent
	End Function

End Class
%>